package com.example.controller;

import com.example.entities.AppUser;
import com.example.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    AccountService accountService;

    @GetMapping(path = "/users")
    public List<AppUser> listUsers(){
        return accountService.listUsers();
    }
}
