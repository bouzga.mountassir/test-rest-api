package com.example.service;

import com.example.entities.AppRole;
import com.example.entities.AppUser;

import java.util.List;

public interface AccountService {
    List<AppUser> listUsers();
}
