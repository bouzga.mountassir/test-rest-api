package com.example.service;

import com.example.entities.AppUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Override
    public List<AppUser> listUsers() {
        List<AppUser> users = List.of(
                new AppUser(1L,"U1","PSWU1",null),
                new AppUser(2L,"U2","PSWU2",null),
                new AppUser(3L,"U3","PSWU3",null));
        return users;
    }
}
